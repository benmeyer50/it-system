	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h3>Help Desks</h3>
				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Active</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($helpdesks as $desk):?>
							<tr>
								<td><?php echo $desk->name;?></td>
								<td><?php if($desk->active == 1) echo "Active"; else echo "Inactive";?></td>
								<?php echo "<td><a href=\"/helpdesk/specific_helpdesk/$desk->id\">Details</td>"?>

							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
			<div class="col-md-8">
				<h3>Active Tickets</h3>
				<?php if($activetickets != null):?>
				<table class="table">
					<thead>
						<tr>
							<th>Subject</th>
							<th>Date Submitted</th>
							<th>Submitted By</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($activetickets as $tickets):?>
							<tr>
								<td><?php echo $tickets->subject;?></td>
								<td><?php echo $tickets->daterequested;?></td>
								<td><?php echo $tickets->requestor;?></td>

								<?php echo "<td><a href=\"/helpdesk/specific_ticket/$tickets->id\">Details</td>"?>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
				<?php else: ?>
					<h4>No Active Tickets</h4>
				<?php endif;?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<a href="/helpdesk/create_helpdesk">Create Helpdesk</a>
			</div>
			<div class="col-md-8">
				<a href="/helpdesk/create_ticket">Create New Ticket</a>
			</div>

		</div>
		<div class="row">
			<div id="ticketChart" style="width: 600px; height=900px;"></div>
		</div>
	</div>
</body>
</html>
