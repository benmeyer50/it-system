	<div class="container">
		<div class="row">
			<h1><?php echo $helpdeskinfo->name?> - <?php if($helpdeskinfo->active == 1) echo "Active"; else echo "Inactive";?><h1>
		</div>
		<div class="row">
			<div class="col-md-8">
				<h3>Active Tickets</h3>
				<?php if($ticketinfo != null):?>
				<table class="table">
					<thead>
						<tr>
							<th>Subject</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($ticketinfo as $tickets):?>
							<tr>
								<td><?php echo $tickets->subject;?></td>
								<?php echo "<td><a href=\"/helpdesk/specific_ticket/$tickets->id\">Details</td>"?>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
				<?php else: ?>
					<h4>No Active Tickets</h4>
				<?php endif;?>
			</div>
		</div>
		<div class="row">
			<div id="ticketChart" style="width: 600px; height=900px;"></div>
		</div>
	</div>
</body>
</html>

