<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userinfo extends CI_Controller {

	public function index()
	{
		$this->load->model('userinfo/adminmod');
		$this->load->model('userinfo/usermod');
	}
}

/* End of file userinfo.php */
/* Location: ./application/controllers/userinfo.php */