<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Helpdesk extends CI_Controller {

	public function index()
	{
		$this->load->model('helpdesk/helpdesksmod');
		$this->load->model('helpdesk/ticketsmod');
		$data['helpdesks'] = $this->helpdesksmod->get_helpdesks();
		$data['activetickets'] = $this->ticketsmod->get_active_tickets();
		$data['open_tickets'] = $this->ticketsmod->num_open_tickets();
		$data['closed_tickets'] = $this->ticketsmod->num_closed_tickets();
		$this->load->view('templates/header');
		$this->load->view('Helpdesk/main', $data);
	}
	public function specific_ticket()
	{
		$this->load->model('helpdesk/ticketsmod');
		$this->load->model('helpdesk/commentsmod');
		$data['ticket_id'] = $this->uri->segment(3,0);
		$ticket_id = $this->uri->segment(3,0);
		$data['ticketinfo'] = $this->ticketsmod->get_ticket($ticket_id);
		$data['comments'] = $this->commentsmod->get_comments_for_ticket($ticket_id);
		$this->load->view('templates/header');
		$this->load->view('Helpdesk/ticket_details', $data);
	}
	public function specific_helpdesk()
	{
		$this->load->model('helpdesk/ticketsmod');
		$this->load->model('helpdesk/helpdesksmod');
		$ticket_id = $this->uri->segment(3,0);
		$data['ticketinfo'] = $this->ticketsmod->get_helpdesk_tickets($ticket_id);
		$data['helpdeskinfo'] = $this->helpdesksmod->get_specific_helpdesk($ticket_id);
		$this->load->view('templates/header');
		$this->load->view('Helpdesk/helpdesk_details', $data);
	}

	public function create_ticket()
	{
		$this->load->helper('form');
		$this->load->view('header');
		$this->load->view('Helpdesk/ticket_create');
	}

	public function create_ticket_action()
	{

	}
}

/* End of file helpdesk.php */
/* Location: ./application/controllers/helpdesk.php */
