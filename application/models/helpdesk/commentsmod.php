<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commentsmod extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	function get_comments_for_ticket($ticket_id)
	{
		$this->db->where('request', $ticket_id);
		$query = $this->db->get('Comments');
		return $query->result();
	}
}

/* End of file commentsmod.php */
/* Location: ./application/models/commentssmod.php */