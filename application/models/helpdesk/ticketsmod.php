<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ticketsmod extends CI_Model {
	
	var $name = '';
	
    function __construct()
    {
        parent::__construct();
    }
	
	function get_active_tickets()
	{
		$this->db->where('completed', FALSE);
		$query = $this->db->get('Tickets');
		if($query->num_rows() > 0)
			return $query->result();
		else
			return null;
	}
	function get_ticket($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('Tickets');
		return $query->row();
	}
	function num_open_tickets()
	{
		$this->db->where('completed', false);
		$query = $this->db->get('Tickets');
		return $query->num_rows();
	}
	function num_closed_tickets()
	{
		$this->db->where('completed', true);
		$query = $this->db->get('Tickets');
		return $query->num_rows();
	}
	function num_tickets()
	{
		$query = $this->db->get('Tickets');
		return $query->num_rows();
	}
	function get_helpdesk_tickets($id)
	{
		$this->db->where('helpdesk', $id);
		$query = $this->db->get('Tickets');
		if($query->num_rows() > 0)
			return $query->result();
		else
			return null;
	}

}

/* End of file ticketsmod.php */
/* Location: ./application/models/ticketsmod.php */
