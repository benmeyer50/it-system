<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Helpdesksmod extends CI_Model {
	
	var $name = '';
	
    function __construct()
    {
        parent::__construct();
    }
	
	function get_helpdesks()
	{
		$query = $this->db->get('HelpDesks');
		return $query->result();
	}
	function get_specific_helpdesk($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('HelpDesks');
		return $query->row();
	}

}

/* End of file heldesksmod.php */
/* Location: ./application/models/helpdesksmod.php */
